/**
 * @file
 * A Backbone view for the toolbar element. Listens to mouse & touch.
 */

(function ($, Drupal) {

  'use strict';

    /**
     * Update the attributes of the toolbar bar element.
     */
  Drupal.toolbar.ToolbarVisualView.prototype.updateBarAttributes = function () {
    var isOriented = this.model.get('isOriented');
    this.$el.find('.toolbar-bar').removeAttr('data-offset-top');
    this.$el.toggleClass('toolbar-oriented', isOriented);
  };

  Drupal.toolbar.BodyVisualView.prototype.render = function () {
    var $body = $('body');
    var orientation = this.model.get('orientation');
    var isOriented = this.model.get('isOriented');
    var isViewportOverflowConstrained = this.model.get('isViewportOverflowConstrained');

    $body
      .toggleClass('toolbar-vertical', (orientation === 'vertical'))
      .toggleClass('toolbar-horizontal', (isOriented && orientation === 'horizontal'))
      .toggleClass('toolbar-fixed', (isViewportOverflowConstrained || this.model.get('isFixed')))
      .toggleClass('toolbar-tray-open', !!this.model.get('activeTray'))
  };


}(jQuery, Drupal));
