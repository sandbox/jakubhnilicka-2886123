<?php

/**
 * @file
 * Defines FormAlters.php.
 */

namespace Drupal\adminic_tools;

use Drupal\Core\Form\FormStateInterface;

class FormAlters {

  /**
   * Alter node_type_add_form form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param $form_id
   *   Form ID.
   */
  public function NodeTypeAddForm(array &$form, FormStateInterface $form_state, $form_id) {
    $this->NodeTypeForm($form, $form_state, $form_id);

    // Disable preview before submitting defaultly.
    $form['submission']['preview_mode']['#default_value'] = 0;
    // Disable Promoted to front page and Create new revision defaultly.
    unset(
      $form['workflow']['options']['#default_value']['promote'],
      $form['workflow']['options']['#default_value']['revision']
    );
    // Don't display author defaultly.
    $form['display']['display_submitted']['#default_value'] = FALSE;
  }

  /**
   * Alter node_type_edit_form form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param $form_id
   *   Form ID.
   */
  public function NodeTypeEditForm(array &$form, FormStateInterface $form_state, $form_id) {
    $this->NodeTypeForm($form, $form_state, $form_id);
  }

  /**
   * Common alters for node_type_form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param $form_id
   *   Form ID.
   */
  public function NodeTypeForm(array &$form, FormStateInterface $form_state, $form_id) {
    // Wrap basic settings to fieldset.
    $form['basic'] = array(
      '#type' => 'fieldset',
      '#title' => t('Basic settings'),
      '#weight' => -99,
    );
    $form['name']['#group'] = 'basic';
    $form['type']['#group'] = 'basic';
    $form['description']['#group'] = 'basic';
  }

  /**
   * Alter views_ui_admin_settings_basic form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param $form_id
   *   Form ID.
   */
  public function ViewsUiAdminSettingBasic(array &$form, FormStateInterface $form_state, $form_id) {
    // Wrap basic settings to details.
    $basic_settings = [
      '#type' => 'details',
      '#title' => t('Basic settings'),
      '#open' => TRUE,
    ];
    $form['basic'] += $basic_settings;
  }

}
